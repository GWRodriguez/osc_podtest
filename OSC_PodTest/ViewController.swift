//
//  ViewController.swift
//  OSC_PodTest
//
//  Created by GW Rodriguez on 10/4/18.
//  Copyright © 2018 GW Rodriguez. All rights reserved.
//

import Cocoa
import SwiftOSC


class ViewController: NSViewController {

    var client: OSCClient!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        client = OSCClient(address: "127.0.0.1", port: 8080)
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }

    @IBAction func sendingIP(_ sender: NSTextField) {
        client = OSCClient(address: sender.stringValue, port: 8080)
    }
    
    @IBAction func sendMessage(_ sender: Any) {
        let mess = OSCMessage(OSCAddressPattern("/something"), 44)
        client.send(mess)
        
        print("message sent")
    }
}

